package observer;

public class AniversarianteSurpresa{
     
    public static void main(String[] args) {
        Namorada namorada = new Namorada();
        Porteiro porteiro = new Porteiro();

        porteiro.addChegadaAniversarianteObserver(namorada);

        // Executando a thread
        porteiro.start();
    }
}