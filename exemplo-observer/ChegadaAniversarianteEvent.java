// Define todas as informações relevantes ao evento, carrega os dados do evento
package observer;

public class ChegadaAniversarianteEvent{
    private final Date horaDachegada;

    public ChegadaAniversarianteEvent(Date horaDaChegada) {
        super();
        this.horaDaChegada = horaDaChegada;
    }

    public Date getHoraChegada() {
        return horaDaChegada;
    }
}