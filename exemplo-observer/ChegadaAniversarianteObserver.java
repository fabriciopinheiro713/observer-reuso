// Todos os interessados e observadores devem implementar o método dessa interface
package observer;
public interface ChegadaAniversarianteObserver {
    public void chegou(ChegadaAniversarianteEvent event);
}